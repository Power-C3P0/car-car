from django.contrib import admin
from .models import Technicians, Appointments, Customer

# Register your models here.

@admin.register(Technicians)
class TechniciansAdmin(admin.ModelAdmin):
    pass

@admin.register(Appointments)
class AppointmentsAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass