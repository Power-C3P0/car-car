
from datetime import datetime
from django.db import models
from django.urls import reverse
# Create your models here.

class Technicians(models.Model):
    employee_number = models.CharField(max_length=50)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_technician", kwargs={"pk": self.id})

class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20, help_text="xxx-xxx-xxxx", unique=True)
    
    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("update_service_customer", kwargs={"pk": self.id})


class Appointments(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    customer_name = models.CharField (max_length=50)
    date = models.DateTimeField(default=datetime.now)
    time = models.CharField(max_length=10)
    technician = models.ForeignKey(Technicians, related_name="Appointments", on_delete=models.CASCADE)
    reason = models.TextField(max_length=1000)
    completed = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)
    

   
    
