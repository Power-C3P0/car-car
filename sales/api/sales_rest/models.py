from django.db import models
from django.urls import reverse
#from phone_field import PhoneField

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default="")
    vin = models.CharField(max_length=17, unique=True)
    
    def __str__(self):
        return self.import_href
    
    
class SalesEmployee(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.IntegerField(unique=True)
    
    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("show_sales_employee", kwargs={"pk": self.id})
    
class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=300)
    phone_number = models.CharField(max_length=20, help_text="xxx-xxx-xxxx", unique=True)
    
    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("update_sales_customer", kwargs={"pk": self.id})
    
    
class SalesRecord(models.Model):
    price = models.FloatField(max_length=100)
    date = models.DateTimeField(auto_now=True)
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_records",
        on_delete=models.PROTECT
    )
    sales_person = models.ForeignKey(
        SalesEmployee, 
        related_name='sales_records', 
        on_delete=models.PROTECT
        )
    customer = models.ForeignKey(
        Customer,
        related_name='sales_records',
        on_delete=models.PROTECT,
    )
    
    def get_api_url(self):
        return reverse("api_show_record", kwargs={"pk": self.pk})
    
    def __str__(self):
        return (str(self.automobile.vin))





    