import React from "react";

class SalesEmployeeHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales_person: ""
        };
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
    }
    handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({
            sales_person: value,
            target_records: []
        })
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Employee Sales History</h1>
                        <div className="mb-3">
                            <select 
                                onChange={this.handleChangeSalesPerson}
                                value={this.state.sales_person}
                                id="sales_person"
                                className="form-select">
                            <option value="">
                                Choose an Employee
                            </option>
                            {this.props.salesEmployees.map(employee => {
                                return (
                                    <option key={employee.id} value={employee.employee_number}>
                                        {employee.name}
                                    </option>
                                );
                            })}      
                            </select>
                        </div>
                    </div>
                </div>
            <div className="row">
                {this.props.salesRecords.filter(record => {
                    if (record.sales_person.employee_number.toString() === this.state.sales_person) {
                        return record;
                    }
                }).map(targetRecord => {
                    return (
                        <div key={targetRecord.id} className="col">
                            <div className="card mb-3 shadow">
                                <div className="card-body">
                                <h5 className="card-subtitle mb-2 text-muted">Date: {targetRecord.date}</h5>
                                <h6 className="card-subtitle mb-2">Price: {targetRecord.price}</h6>
                                <p className="card-text">
                                    Vin Number: {targetRecord.automobile.vin}
                                </p>
                                <p className="card-text">
                                    Customer: {targetRecord.customer.name}
                                </p>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
            </div>
        )
    }
}
export default SalesEmployeeHistory;