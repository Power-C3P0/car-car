function SalesEmployeeList({salesCustomers}) {
    
    return (
        <div className="container">
            <h2 className="display-5 fw-bold">Customer</h2>
            <a href='new/'>
                <h5>Add Customer</h5>
            </a>
            <div className="row">
                {salesCustomers.map(customer => {
                    return (
                        <div key={customer.id} className="col">
                            <div className="card mb-3 shadow">
                                <div className="card-body">
                                    <a href={'/salesCustomers/' + customer.id}>
                                        <h5 className="card-subtitle mb-2 text-muted">
                                        {customer.name}
                                        </h5>
                                    </a>
                                    <h6 className="card-subtitle mb-2 text-muted"> Phone Number: {customer.phone_number}
                                    </h6>
                                    <p>
                                        {customer.address}
                                    </p>
                                </div>
                            </div>
                        </div>
                    );
                })}    
            </div>        
        </div>
    );
}
export default SalesEmployeeList