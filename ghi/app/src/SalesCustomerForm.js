import React from "react";

class SalesCustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            phone_number: "",
            address: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const locationUrl = 'http://localhost:8090/api/sales_customers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            const newCustomer = await response.json();
            console.log(newCustomer);
            this.setState({
                name: '',
                phone_number: '',
                address: ''
            })
        }
    }
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value});
    }
    handleChangePhoneNumber(event) {
        const value = event.target.value;
        this.setState({phone_number: value})
    }
    handleChangeAddress(event) {
        const value = event.target.value;
        this.setState({address: value});
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new Customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type = "text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangePhoneNumber} value={this.state.phone_number} placeholder="Phone Number" required type = "text" name="phone_number" id="phone_number" className="form-control" />
                                <label htmlFor="phone_number">Phone Number</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeAddress} value={this.state.address} placeholder="Address" required type = "text" name="address" id="address" className="form-control" />
                                <label htmlFor="address">Address</label>
                            </div>
                            <button className="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
export default SalesCustomerForm;