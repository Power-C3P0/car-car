import React from "react";

class ServiceHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            targetVin: "",
    
        };
        this.handleChangeTargetVin = this.handleChangeTargetVin.bind(this);
    }
    handleChangeTargetVin(event) {
        const value = event.target.value;
        this.setState({
            targetVin: value,
        
        })
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Service History</h1>
                        <div className="mb-3">
                            <select 
                                onChange={this.handleChangeTargetVin}
                                value={this.state.targetVin}
                                id="targetVin"
                                className="form-select">
                            <option value="">
                                Choose a Vin
                            </option>
                            {this.props.serviceAppointments ? this.props.serviceAppointments.map(vinRecord => {
                                return (
                                    <option key={vinRecord.id} value={vinRecord.vin}>
                                        {vinRecord.vin}
                                    </option>
                                );
                            }):null}      
                            </select>
                        </div>
                    </div>
                </div>
            <div className="row">

                {this.props.serviceAppointments.filter(record => {
                    if (record.completed === true && record.vin === this.state.targetVin) {
                        return record;
                    }
                }).map(targetRecord => {
                    return (
                        <div key={targetRecord.id} className="col">
                            <div className="card mb-3 shadow">
                                <div className="card-body">
                                <h5 className="card-subtitle mb-2 text-muted">Date: {targetRecord.date}</h5>
                                <h6 className="card-subtitle mb-2">Price: {targetRecord.price}</h6>
                                <p className="card-text">
                                    Vin Number: {targetRecord.vin}
                                </p>
                                <p className="card-text">
                                    Customer: {targetRecord.customer_name}
                                </p>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
            </div>
        )
    }
}
export default ServiceHistory;