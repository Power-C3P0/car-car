import React from 'react';

class AppointmentsForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            customer_name: "",
            date: "",
            time: "",
            technician: "",
            technicians:[],
            reason: "",
            completed: false,
            vip: false,
        };
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleVipChange = this.handleVipChange.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
    
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({technicians: data});
        }

      }
        
 
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
         delete data.technicians

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
        const newAppointment = await response.json();
        console.log(newAppointment);
        
        {
            const cleared = {
            vin: "",
            customer_name: "",
            date: "",
            time: "",
            technician: "",
            reason: "",
            completed: false,
            vip: false,
        };
        this.setState(cleared);
        }
    }
    }   

    handleVinChange(event){
        const value = event.target.value;
        this.setState({vin: value})
    }
    handleCustomerNameChange(event) {
        const value = event.target.value;
        this.setState({customer_name: value})
    }   
    handleDateChange(event) {
        const value = event.target.value;
        this.setState({date: value})
    }   
    handleTimeChange(event) {
        const value = event.target.value;
        this.setState({time: value})
    }
    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({technician: value})
    }   
    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({reason: value})
    }
    handleVipChange(event) {
        const value = event.target.value;
        this.setState({vip: value})
    }
    vipTrue() {
        this.setState({vip: "True"})
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Make a new Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleVinChange} value={this.state.vin} placeholder="Vin" required type = "text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">Vin</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCustomerNameChange} value={this.state.customer_name} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name" className="form-control"/>
                                    <label htmlFor="customer_name">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleDateChange} value={this.state.date_time} placeholder="date" required type="date" name="date" id="date" className="form-control"/>
                                <label htmlFor="date_time">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleTimeChange} value={this.state.date_time} placeholder="time" required type="time" name="time" id="time" className="form-control"/>
                                <label htmlFor="date_time">Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={this.handleTechnicianChange} value={this.state.technician} required name="technician" id="technician" className="form-select">
                                    <option value="">Choose a Technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                        <option key={technician.employee_number} value={technician.employee_number}>{technician.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="Reason" required type = "text" name="reason" id="reason" className="form-control" />
                                <label htmlFor="reason">Reason</label>
                            </div>

                            <button className="btn btn-primary">Add</button>
                        </form>
                        <div>
                            {this.props.salesRecords.filter(record => {
                                if(this.state.vin in record.automobile){
                                    return record
                                }
                            }).map(vipChange => {
                                return (
                                    <h3 key={vipChange.id}>This VIN is VIP</h3>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }


}
export default AppointmentsForm;