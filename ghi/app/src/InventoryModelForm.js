import React from "react";

class InventoryModelForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            picture_url: "",
            manufacturer: "",
            manufacturers: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this)
        this.handelChangeManufacturer = this.handelChangeManufacturer.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers})
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers;
        data.manufacturer_id = data.manufacturer;
        delete data.manufacturer;

        const locationUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };
        console.log(data);
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newModel = await response.json();
            console.log(newModel);
            this.setState({
                name: "",
                picture_url: "",
                manufacturer: "",
            })
        }
    }
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({name: value});
    }
    handleChangePictureUrl(event) {
        const value = event.target.value;
        this.setState({picture_url: value});
    }
    handelChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({manufacturer: value});
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form=floating mb-3">
                                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form=floating mb-3">
                                <input onChange={this.handleChangePictureUrl} value={this.state.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handelChangeManufacturer}  value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Choose an Manufacturer</option>
                                {this.state.manufacturers.map(make => {
                                    return (
                                        <option 
                                            key={make.id} 
                                            value={make.id}
                                        >
                                            {make.name}
                                        </option>
                                    );
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default InventoryModelForm;